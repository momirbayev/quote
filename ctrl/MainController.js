angular.module("QuoteApp", ['ui.bootstrap']).controller("MainController", function ($scope, $http) {
    $scope.quotes = [];
    $scope.addQuote = function () {
        if ($scope.quotes.length < 10) {
            if ($scope.newQuote && $scope.newQuote.length > 0) {
                $scope.quotes.push($scope.newQuote);
                $scope.newQuote = null;
                $scope.save();
            } else {
                $scope.error = "Напишите текст в окно для заметки";
            }
        } else {
            $scope.error = "Для добавления новых цитат удалите одну из добавленных";
        }
    };
    $scope.closeAlert = function () {
        $scope.error = null;
    };
    $scope.remove = function (index) {
        $scope.quotes.splice(index, 1);
        $scope.save();
    };
    $scope.save = function(){
        $http.post('save.php', $scope.quotes).then(function() {
        },function(){
            $scope.error = "Ошибка при записи в файл";
        });
    };
    $scope.init = function () {
        $http.get('quotes.json').then(function (response) {
            if(response.data && response.data.length>0)
            $scope.quotes = response.data;
        }, function (response) {
            $scope.error = "Ошибка при считывании файла";
        });
    };
    $scope.init();
});